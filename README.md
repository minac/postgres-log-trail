# PostgreSQL lock logs

### Setup

1) Clone this project into your local development environment
2) Clone https://github.com/postgres/postgres into `database` folder
3) **(Optional)** Checkout relevant version of the `postgres` (e.g. `cd database/postgres && git checkout REL_12_0`)
4) Run `make build` to build images
5) Create `structure.sql` file in `database/data` folder
6) Create `test.sql` file in `database/data` folder

### Usage:

1) Run `make up` to spinoff the containers
2) Run `make schema-load` to execute the `structure.sql`
3) Run `make run-sql` to execute the content of the `test.sql`

Check all available make rules by running `make`.
