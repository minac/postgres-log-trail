DC=docker-compose -f docker-compose.yml $(1)
DC-RUN=$(call DC, run --rm trailer $(1))
DC-EXEC=$(call DC, exec database $(1))

usage:
	@echo "Available targets:"
	@echo "  * build               - Builds images"
	@echo "  * up                  - Runs the trailer"
	@echo "  * dev                 - Opens an ash session in the trailer container"
	@echo "  * schema-load         - Loads the structure.sql"
	@echo "  * run-sql             - Runs the SQL queries"
	@echo "  * down                - Removes all the containers"
	@echo "  * tear-down           - Removes all the containers with volumes and tears down the setup"

build:
	$(call DC, build)

up:
	$(call DC, up)

dev:
	$(call DC-RUN, ash)

schema-load:
	$(call DC-EXEC, psql postgresql://postgres:postgres@localhost:5432/postgres -f /data/structure.sql)

run-sql:
	$(call DC-EXEC, psql postgresql://postgres:postgres@localhost:5432/postgres -f /data/test.sql)

down:
	$(call DC, down)

tear-down:
	$(call DC, down -v --rmi all --remove-orphans)
