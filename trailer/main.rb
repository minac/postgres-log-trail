# frozen-string-literal: true

require "bundler/inline"

gemfile(true) do
  source "https://rubygems.org"

  gem "file-tail", "~> 1.2"
  gem "pg", "1.4.1"
end

require "pg"

LOG_REGEX = /LOG\:\s+LockAcquire\:\s+lock\s+\[\d+,(\d+)\]/
SQL_FORMAT = "SELECT relname FROM pg_class WHERE oid = %<oid>d"

connection = PG.connect("postgresql://postgres:postgres@database:5432/postgres")

puts "Listening logs..."

file = File.open("/trail_logs/logfile").extend(File::Tail)

file.tail do |log_line|
  match_data = log_line.match(LOG_REGEX)

  next unless match_data

  puts format(SQL_FORMAT, oid: match_data.captures.first).then { |sql| connection.exec(sql) }
                                                         .then { |result_set| result_set.to_a.first&.fetch("relname") }
                                                         .then { |relname| log_line.sub(/LockAcquire/, "LockAcquire(#{relname})") }
end
